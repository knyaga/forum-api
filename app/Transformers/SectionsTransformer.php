<?php
/**
 * Created by PhpStorm.
 * User: otis
 * Date: 2016-08-03
 * Time: 09:39
 */

namespace App\Transformers;


use App\Models\Section;
use League\Fractal\TransformerAbstract;

class SectionsTransformer extends TransformerAbstract
{
    public function transform(Section $section)
    {
        return [
            'id' => $section->id,
            'title' => $section->title,
            'description' => $section->description
        ];
    }

}