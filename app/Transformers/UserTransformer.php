<?php
/**
 * Created by PhpStorm.
 * User: otis
 * Date: 2016-08-03
 * Time: 09:01
 */

namespace App\Transformers;


use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        $hash = md5($user->email);

        return [
            'username' => $user->username,
            'avatar'   => 'https://www.gravatar.com/avatar/'.$hash
        ];
    }

}