<?php

namespace App\Http\Controllers\Forum;

use App\Http\Controllers\Controller;
use App\Models\Section;
use App\Models\Topic;
use Illuminate\Http\Request;

class TopicsController extends Controller
{
    public function index(Request $request, Section $section)
    {
        dd("Show all topics within sections");
    }

    public function show(Topic $topic)
    {
        dd("Single topic");
    }

    public function store(Request $request)
    {
        dd("create new topic");
    }
}
