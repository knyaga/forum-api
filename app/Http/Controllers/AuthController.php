<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\SignupFormRequest;
use App\Models\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class AuthController extends Controller
{
    public function register(SignupFormRequest $request)
    {
        User::create([
            'username' => $request->json("username"),
            'email'    => $request->json("email"),
            'password' => bcrypt($request->json("password"))
        ]);
    }

    public function login(Request $request)
    {
       //Obtain user credentials
        $credentials = $request->only("email","password");
        try {
            if(! $token = JWTAuth::attempt($credentials,[
                    'exp' => Carbon::now()->addWeek()->timestamp
            ]))
            {
                return response()->json([
                   'error' => 'Invalid credentials provided'
                ], 401);
            }
        }catch (JWTException $e) {
            return response()->json([
               'error' => 'Could not generate token'
            ], 500);
        }
        return response()->json([
            'token' => $token
        ]);
    }


}
