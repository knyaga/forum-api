<?php

Route::group(['middleware' =>['api']], function(){

    Route::post('/auth/register',[
        'uses' => 'AuthController@register'
    ]);

    Route::post('/auth/login',[
        'uses' => 'AuthController@login'
    ]);

    Route::get('/sections', [
       'uses' => 'Forum\SectionsController@index'
    ]);

    Route::get('/topic', [
       'uses' => 'Forum\TopicsController@index'
    ]);

    Route::get('/topic/{topicId}', [
        'uses' => 'Forum\TopicsController@show'
    ]);

    Route::group(['middleware' => 'jwt.auth'], function (){
        Route::get('/user', [
            'uses' => 'UserController@index'
        ]);

        Route::post('/topic', [
           'uses' => 'Forum\TopicsController@store'
        ]);
    });
});